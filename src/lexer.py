import logging
from dataclasses import dataclass
from enum import IntEnum, unique
from typing import Iterator

logger = logging.getLogger(__name__)

@unique
class TokenKind(IntEnum):
    NONE = -100 # should never be used

    EOF = -1
    NEWLINE = 0

    NUMBER = 2

    PLUS = 101
    MINUS = 102

@dataclass
class Token:
    text: str
    kind: TokenKind

    def __repr__(self):
        return f"Text: {self.text}; Kind: {self.kind};"

class Lexer:
    def __init__(self, source: str) -> None:
        self.source = source + '\n'
        self.curPos = -1
        self.curChar = ''
        self.nextChar()

    def nextChar(self)-> None:
        self.curPos += 1
        if self.curPos >= len(self.source):
            self.curChar = '\0'
        else:
            self.curChar = self.source[self.curPos]

    def peek(self)-> str:
        if self.curPos + 1 >= len(self.source):
            return '\0'
        return self.source[self.curPos + 1]

    def abortMsg(self, message: str) -> str:
        return message + ' at position ' + str(self.curPos)

    def skipWhitespace(self)-> None:
        while self.curChar == ' ' or self.curChar == '\t' or self.curChar == '\r' or self.curChar == '\n':
            self.nextChar()

    def skipComment(self)-> None:
        if self.curChar == '#':
            while self.curChar != '\n':
                self.nextChar()

    def getToken(self)-> Token:
        token = Token('', TokenKind.NONE)
        self.skipWhitespace()
        self.skipComment()
        try:
            if self.curChar == '\0':
                token = Token('', TokenKind.EOF)
            elif self.curChar.isdigit():
                startPos = self.curPos
                while self.peek().isdigit():
                    self.nextChar()
                if self.peek() == '.':
                    self.nextChar()
                    if not self.peek().isdigit():
                         msg = self.abortMsg(' expected digit after digit. Found: ' + self.curChar)
                         raise ValueError(msg)
                    while self.peek().isdigit():
                        self.nextChar() 
                token = Token(self.source[startPos : self.curPos +1], TokenKind.NUMBER )
            elif self.curChar == '+':
                token = Token(self.curChar, TokenKind.PLUS) 
            elif self.curChar == '-':
                token = Token(self.curChar, TokenKind.MINUS)
            else:
                msg = self.abortMsg(' found unexpected character: ' + self.curChar)
                raise ValueError(msg)

        except Exception as e:
                logging.error(str(e))
                raise e

        self.nextChar()
        return token

    def __iter__(self)->Iterator[Token]:
        while (token:= self.getToken()).kind != TokenKind.EOF:
            yield token 
        yield token

    def __repr__(self)-> str:
        return str(self.getToken())

if __name__ == "__main__":
    code = "3+5"
    lexer = Lexer(code)
    print(code)
    result =[
        Token("3",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("5",TokenKind.NUMBER),
        Token("/0",TokenKind.EOF),
    ]

    print(list(lexer) == result)
    # while (tok := lexer.getToken()).kind != TokenKind.EOF:
        # print(tok) 
