import tree as AST
from lexer import *

class Parser:
    def __init__(self, lexer):
        self.tokens = lexer
        self.curToken = Token('', TokenKind.NONE)
        self.peekToken = Token('', TokenKind.NONE)
        self.nextToken()
        self.nextToken()

    def nextToken(self):
        self.curToken = self.peekToken
        self.peekToken = self.tokens.getToken()

    def eat(self, expected_type):
        token = self.curToken
        if self.curToken.kind != expected_type:
            self.abort(f"Expected type {expected_type}  while received {token.kind}")
        self.nextToken()
        return token

    def checkPeek(self, expected_type):
        return self.peekToken.kind == expected_type

    def abort(self, message):
        raise ValueError(f"Error while parsing at: {self.tokens.curPos}: " + message)
    
    def parseBinOp(self):
        result = None
        if self.curToken.kind == TokenKind.NUMBER:
            result = self.eat(TokenKind.NUMBER)
            result = AST.Num(result.text)
            while (_type := self.curToken.kind) == TokenKind.PLUS or _type == TokenKind.MINUS:
                op = "+" if _type == TokenKind.PLUS else "-"
                self.eat(_type)
                right = self.parseBinOp()
                return AST.BinOp(result, op,  right)
            return result
        else:
            msg = 'Expected number or operator. Found: ' + self.curToken.text
            raise ValueError(f"Error while parsing at: {self.tokens.curPos}: " + msg)

    def parse_program(self):
        comp = self.parseBinOp()
        self.eat(TokenKind.EOF)
        return comp

def printAST(tree: AST.Expr| Token, depth: int = 0) -> None: 
    indent = "    " * depth
    node_name = tree.__class__.__name__
    match tree:
        case AST.BinOp(left, op, right):
            print(f"{indent}{node_name}(\n{indent}    {op!r},")
            printAST(left, depth + 1)
            print(",")
            printAST(right, depth + 1)
            print(f",\n{indent})", end="")
        case AST.Num(value):
            print(f"{indent}{node_name}({value!r})", end="")
        case _:
            raise RuntimeError(f"Can't print a node of type {node_name}")
    if depth == 0:
        print()

if __name__ == "__main__":
    code = "1+2-3"
    lexer = Lexer(code)
    parser = Parser(lexer)
    printAST(parser.parse_program())
