import sys, logging
logger = logging.getLogger(__name__)

def run_repl():
    logger.info("Initializing REPL")
    while True:
        command = input("pyl>")
        print(f" The command was: {command}")
        if command == "exit":
            print("Closing REPL")
            logger.info("Closing REPL")
            sys.exit(0)


