# A Python compiler for a small language

This is a compiler for a small language, written in Python.
The small language will be defined in the future. 
For now, I have only set up the project structure and the basics.

This is a small personal project where I hope to apply the new acquired software skills during my PhD in Computer Science at Aalborg University.


## Usage
To compiler a file, use the following command:
```shell
$ python3 pylang.py <filename>
```
To access the REPL, use the following command:
```shell
$ python3 pylang.py
pyl> 
```

## References
- Crafting Interhttps://www.craftinginterpreters.com/
- Let's make a Teeny Tiny compiler: https://austinhenley.com/blog/teenytinycompiler1.html
- golf-compiler: https://github.com/Zelatrix/golf-compiler
- Writing your own programming languange and compiler with Python: https://medium.com/@marcelogdeandrade/writing-your-own-programming-language-and-compiler-with-python-a468970ae6df
- My First Language Frontend with LLVM: https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html
- Building a python compiler and interpreter: https://mathspp.com/blog/building-a-python-compiler-and-interpreter


## License
MIT
