import sys, logging
from src.repl import run_repl

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    if len(sys.argv[1:]) > 1:
        print("Usage: pylang.py <name>")
        logger.error("Too many arguments")
        sys.exit(1)
    elif len(sys.argv[1:]) == 1:
        # compile(sys.argv[1])
        print(f"I would compile {sys.argv[1]}")
        sys.exit(0)
    else:
        run_repl()


