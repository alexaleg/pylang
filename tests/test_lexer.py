import pytest
from lexer import Token, Lexer, TokenKind

def test_tokenizer_addition():
    tokens = list(Lexer("3 + 5"))
    assert tokens == [
        Token("3",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("5",TokenKind.NUMBER),
        Token('',TokenKind.EOF),
    ]

def test_tokenizer_subtraction():
    tokens = list(Lexer("3 - 6"))
    assert tokens == [
        Token("3",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("6",TokenKind.NUMBER),
        Token("",TokenKind.EOF),
    ]

def test_tokenizer_additions_and_subtractions():
    tokens = list(Lexer("1 + 2 + 3 + 4 - 5 - 6 + 7 - 8"))
    assert tokens == [
        Token("1",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("2",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("3",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("4",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("5",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("6",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("7",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("8",TokenKind.NUMBER),
        Token("",TokenKind.EOF),
    ]

def test_tokenizer_additions_and_subtractions_with_whitespace():
    tokens = list(Lexer("     1+       2   +3+4-5  -   6 + 7  - 8        "))
    assert tokens == [
        Token("1",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("2",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("3",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("4",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("5",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("6",TokenKind.NUMBER),
        Token("+",TokenKind.PLUS),
        Token("7",TokenKind.NUMBER),
        Token("-",TokenKind.MINUS),
        Token("8",TokenKind.NUMBER),
        Token("",TokenKind.EOF),
    ]

def test_tokenizer_raises_error_on_garbage():
    with pytest.raises(ValueError):
        list(Lexer("$"))

@pytest.mark.parametrize(
    ["code", "token"],
    [
        ("+", Token("+",TokenKind.PLUS)),
        ("-", Token("-",TokenKind.MINUS)),
        ("3", Token("3",TokenKind.NUMBER)),
    ],
)
def test_tokenizer_recognises_each_token(code: str, token: Token):
    tokens = list(Lexer(code))
    assert tokens == [token, Token("",TokenKind.EOF)]
